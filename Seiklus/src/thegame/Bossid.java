package thegame;

/**
 * Created by Martin on 8.12.2015.
 */

import java.util.Random;

/**
 * Bosside jaoks loodud klass.
 */
public class Bossid {

    /**
     * Suvalise numbri tekitamiseks rand objekt,
     * mida kasutame hiljem bossi nime saamisel.
     */
    Random rand = new Random();

    /**
     * Bossi klassi atribuudid/parameetrid, kus nimed on pandud massiivi,
     * tehtud bossi elude muutuja ning ka rünnaku muutuja.
     */
    String[] bossiNimed = {"Draakon", "Troll", "J.P"};
    String nimi;
    int bossElu;
    int bossRynnak;

    /**
     * Bossid klassikonstruktor, kus anname atribuutidele väärtused.
     */
    public Bossid() {
        this.nimi = bossiNimed[rand.nextInt(bossiNimed.length)];
        this.bossElu = rand.nextInt(50) + 100;
        this.bossRynnak = rand.nextInt(10) + 15;
    }

    /**
     * Meetodid, et saada kätte bossi atribuutide väärtused, ehk "getterid".
     *
     * @return
     */
    public String getNimi() {
        return this.nimi;
    }

    public int getBossElu() {
        return this.bossElu;
    }

    public int getBossRynnak() {
        return this.bossRynnak;
    }

    /**
     * Kuna ainuke asi, mis võitluse jooksul muutuda saab on bossielu,
     * siis loome vaid ühe "setteri".
     *
     * @param bossElu
     */
    public void setBossElu(int bossElu) {
        this.bossElu = bossElu;
    }
}

// Klassi lõpp