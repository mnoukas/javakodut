package thegame;

/**
 * Created by Martin on 22.11.2015.
 */

/**
 * Kangelase jaoks loodud klass.
 */
public class Kangelane {

    /**
     * Kangelase atribuudid/muutujad.
     */
    String nimi;
    int elu;
    int mana;
    int rynnak;
    int maagiaRynnak;
    int parandus;
    boolean maagiaRynnakuArv = true;

    /**
     * Kangelase klassi konstruktor
     *
     * @param nimi
     * @param elu
     * @param rynnak
     * @param maagiaRynnak
     * @param parandus
     * @param mana
     */
    public Kangelane(String nimi, int elu, int rynnak, int maagiaRynnak, int parandus, int mana) {
        this.nimi = nimi;
        this.elu = elu;
        this.rynnak = rynnak;
        this.maagiaRynnak = maagiaRynnak;
        this.parandus = parandus;
        this.mana = mana;
    }

    /**
     * Meetod, mis näitab kasutajale kangelase omadusi ning
     * annab kasutajale teada, et tal on 1 maagiarünnak.
     */
    public void prindiMind() {
        System.out.println("Sinu kangelase nimi on " + this.nimi + ". Sul on " + elu + " elupunkti" + " ning " + this.mana + " manat.");
        System.out.println("Sa suudad vastast lüüa " + this.rynnak + " tugevusega!");
        System.out.println("Sul on ainult 1 maagiarünnak, kasuta seda ainult siis, kui väga vaja.");
    }

    /**
     * Meetodid, et saada kätte kangelase atribuudid, ehk "getterid".
     */
    public String getNimi() {

        return this.nimi;
    }

    public int getElu() {

        return this.elu;
    }

    public int getMana() {

        return this.mana;
    }

    public int getRynnak() {
        return this.rynnak;
    }

    public int getMaagiaRynnak() {
        return this.maagiaRynnak;
    }

    public int getParandus() {
        return this.parandus;
    }

    public boolean getMaagiaRynnakuArv() {
        return this.maagiaRynnakuArv;
    }

    /**
     * Kuna kangelane osaleb nii võitluses, mana tagasisaamises,
     * elu tagasisaamises, siis on talle vaja teha mitmeid "settereid".
     */
    public void setNimi(String nimi) {
        this.nimi = nimi;
    }

    public void setElu(int elu) {
        this.elu = elu;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public void setRynnak(int rynnak) {
        this.rynnak = rynnak;
    }

    public void setMaagiaRynnak(int maagiaRynnak) {
        this.maagiaRynnak = maagiaRynnak;
    }

    public void setParandus(int parandus) {
        this.parandus = parandus;
    }

    public void setMaagiaRynnakuArv(boolean maagiaRynnakuArv) {
        this.maagiaRynnakuArv = maagiaRynnakuArv;
    }
}

// Klassi lõpp

