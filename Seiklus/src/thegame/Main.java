package thegame;

import java.util.Random;
import java.util.Scanner;

/**
 * Mäng seisneb ühe mehe seiklemises läbi koopa, kus ette tulevad igasugused kollid
 * ja bossid, kellel on erinevad rünnakutugevused ning elud. Kas Sina suudad ellu jääda?
 * Created by Martin on 22.11.2015.
 */

public class Main {

    // Suvalise numbri generaatori objekt
    static Random rand = new Random();

    /**
     * Globaalsed muutujad.
     * bossSpawner, mis mõjutab kas bossi või tavalise vastase teket.
     * bossUuedElud, mis mõjutab võimalust, et boss saaks ennast tervendada.
     * skoor, mängu alguses skoor on alati 0, lõpuks liidetakse kokku vastavalt tapetud vastastele ja bossidele.
     * bossKills, loeb mitu bossi kangelane tappis.
     * vastaneKills, loeb mitu vastast kangelane tappis.
     * kontroll, mõjutab gameOver meetodit, mis läheb false-iks, kui kasutaja sisestab vale numbri
     * ning tänu sellele küsib kasutajalt uuesti, et ta õige numbri sisestaks.
     */
    static int bossSpawner = rand.nextInt(50);
    static int bossUuedElud = rand.nextInt(20);
    static int skoor = 0;
    static int bossKills = 0;
    static int vastaneKills = 0;
    static boolean kontroll = true;
    public static Bossid boss;
    public static Vastased vastane;
    public static Kangelane hero;


    //Mängu algus
    public static void main(String[] args) {

        /* Mängu käivitamine meetodiga, mis laseb kasutajal sisestada nime
         *ning annab teada Sinu kangelase atribuudid. Tekitatakse kangelase objekt.
         **/
        gameStart();

        // Meetod, kus toimub võitlus nii kaua, kuni kangelane on elus.
        theGame();

        // Meetod, mis annab mängijale võimaluse peale kangelase surma, kas mängida edasi või lõpetada mäng.
        gameOver();
    }

    // Võitluse meetod
    public static void theGame() {
        while (hero.getElu() >= 1) {

            // Kolli või bossi tekkimine
            if (bossSpawner > 35) {

                // Loome uue boss objekti, kasutades globaalseid muutujaid ning neil kõigil on random väärtus.
                boss = new Bossid();
                System.out.println("Boss leidis sinu üles! Seekord siis: " + boss.getNimi() + ". Elupunkte: " + boss.getBossElu() + " Löök: " + boss.getBossRynnak());
            } else {

                // Loome uue vastase, kasutades globaalseid muutujaid ning ka neil on random väärtused.
                vastane = new Vastased();
                System.out.println("Sinu ette ilmus olend: " + vastane.getNimi() + ". Elupunkte: " + vastane.getElupunktid() + " Löök: " + vastane.getTabamus());
            }

            System.out.println("_____________________________________________________");

            /** Tsükkel, mis kontrollib, kas boss/koll on elus ning kas kangelane on elus.
             * Kui kõik klapib, siis käib võitlus.
             */
            while ((((bossSpawner > 35 && boss.getBossElu() >= 1)) || ((bossSpawner < 35) && (vastane.getElupunktid() >= 1))) && hero.getElu() >= 1) {

                // Mängija valikuvõimaluste nimekiri
                System.out.println("Mida sa teha soovid?");
                System.out.println("1: Rünnata");
                System.out.println("2: Ennast tervendada");
                System.out.println("3: Kasutada maagiarünnakut");
                System.out.println("_____________________________________________________");
                Scanner input = new Scanner(System.in);

                /** Üritab völtida programmi kokkujooksmist sellega, et
                 * kontrollib, kas kasutaja sisestab tähemärgi. Kui jah, siis
                 * annab teada, et arvu asemel sisestati tähemärk.
                 **/
                try {
                    int valik = input.nextInt();
                    switch (valik) {
                        case 1:
                            sinuRynnak();
                            vastaseRynnak();
                            kuiPaljuTapsid();
                            surmaKontroll();
                            võiduKontroll();
                            skooriArvutus();
                            break;
                        case 2:
                            parandus();
                            break;
                        case 3:
                            magicRynnak();
                            kuiPaljuTapsid();
                            võiduKontroll();
                            break;
                        default:
                            System.out.println(" Vale arv!");
                    }
                } catch (Exception e) {
                    System.out.println("Sisesta arv, mitte täht.");
                }
            }
        }
        bossSpawner = rand.nextInt(50);
    }

    /**
     * Mängu käivitamise meetod, millega luuakse uus kangelane.
     * Kasutajal/mängijal tuleb sisestada kangelase nimi.
     * Kangelane tekib suvalise rünnakutugevusega ning suvalise maagiarünnakutugevusega.
     * Meetodi lõpus on omakorda meetod Kangelased klassis, mis prindib kasutajale/mängijale tema kangelase andmed.
     */
    public static Kangelane gameStart() {
        Scanner input = new Scanner(System.in);
        Random rand = new Random();
        System.out.println("Oled valmis elu eest võitlema?");
        System.out.println("Sisesta oma kangelase nimi: ");
        hero = new Kangelane(input.nextLine(), 150, rand.nextInt(40) + 15, rand.nextInt(100), 45, 50);
        System.out.println("Mängujooksul peetakse skoori, vastane annab 5 punkti ning boss 10 punkti.");
        System.out.println("The game is about to start.");
        hero.prindiMind();
        return hero;
    }

    /**
     * Kangelase rünnakumeetod, mis lihtsalt ründab ette tekkinud vastast.
     * Kangelase ette võib tekkida kas boss või tavaline vastane, selle tagab bossSpawner muutuja
     * Rünnatakse bossi.
     * Bossil on väike võimalus, et ta saab elusi juurde, selle tagab bossiUuedElud meetod.
     * Kui boss saab surma, siis käivitub bossiSurmaKontroll meetod, mis annab mängijale teada, et boss on alistatud.
     * Kui bossSpawner on aga väiksem kui 40, siis tekib tavaline vastane.
     * Rünnatakse vastast.
     * Meetod vastaseSurmaKontroll annab kasutajale teada, et vastane sai surma.
     */
    public static void sinuRynnak() {
        if (bossSpawner > 35) {
            boss.setBossElu(boss.getBossElu() - hero.getRynnak());
            bossiUuedElud();
            bossiSurmaKontroll();
        } else if (bossSpawner < 35) {
            vastane.setElupunktid(vastane.getElupunktid() - hero.getRynnak());
            vastaseSurmaKontroll();
        }
    }

    /**
     * Vastaserünnaku meetod.
     * Kui on tekkinud boss ning ta on elus, siis ta suudab kangelast rünnata.
     * Kui boss sai su elud alla ühe, siis antakse kasutajale teada, et sinuga on lõpp peal.
     * Kui boss surma saab, siis enam ta sulle viga teha ei saa.
     * Kui on tekkinud tavaline vastane ning ta on elus, siis ta suudab samuti kangelast rünnata.
     * Vastasel on sama teavitaja, mis annab kasutajale teada, kui vastane sai sinust jagu.
     * Kui vastane saab surma ning kangelasel on kas 25 või rohkem elu, siis suudab vastane kangelasele ikkagi viga teha.
     */
    public static void vastaseRynnak() {
        if (bossSpawner > 35 && boss.getBossElu() >= 1) {
            hero.setElu(hero.getElu() - boss.getBossRynnak());
            if (hero.getElu() >= 1)
                System.out.println("Boss ründas sind tugevalt, sul on alles " + hero.getElu() + " elu!");
            if (hero.getElu() < 1)
                System.out.println("Boss tegi sulle surmava löögi.");
        } else if (bossSpawner > 35 && boss.getBossElu() < 1) {
            System.out.println("Rohkem boss sulle õnneks pihta ei saanud.");
        } else if (bossSpawner < 35 && vastane.getElupunktid() >= 1) {
            hero.setElu(hero.getElu() - vastane.getTabamus());
            if (hero.getElu() >= 1)
                System.out.println("Vastane ründas sind, sul on alles " + hero.getElu() + " elu!");
            if (hero.getElu() < 1)
                System.out.println("Vastane lõi sind surmavalt!");
        } else if ((bossSpawner < 35 && vastane.getElupunktid() < 1) && ((hero.getElu() >= 25))) {
            hero.setElu(hero.getElu() - rand.nextInt(20) + 1);
            System.out.println("Ta siiski suutis sind riivata.");
            System.out.println("Sul on alles " + hero.getElu() + " elu.");
        }
    }

    /**
     * Kangelasel on võimalus ennast tervendada, selleks ka parandus meetod.
     * Kui kangelasel on rohkem kui 30 manat, siis ta saab ennast tervendada, lisades endale juurde 30 elu.
     * Elude tagasitulekul väheneb mana aga 20 võrra.
     * Kui kangelane tahab ennast tervendada, kuid manat ei jätku, siis saab kasutaja sellest teavituse.
     */
    public static void parandus() {
        if (hero.getMana() > 30) {
            hero.setElu(hero.getElu() + 30);
            System.out.println("Sa kasutasid oma manat ning said juurde 30 elu!");
            hero.setMana(hero.getMana() - 20);
            System.out.println("Sul on alles " + hero.getMana() + " manat ning " + hero.getElu() + " elu!");
        } else {
            System.out.println("Sul on vaja rohkem manat, hetkeseis on " + hero.getMana() + " manat!");
        }
    }

    /**
     * Kangelase mana tagasituleku meetod.
     * Algselt on kangelasel 50 manat.
     * Kui kangelane saab bossist jagu, siis saab ta juurde 20 manat.
     * Kui kangelane saab jagu vastasest, siis saab ta juurde 10 manat.
     */
    public static void uusMana() {
        if (bossSpawner > 35 && boss.getBossElu() < 1) {
            hero.setMana(hero.getMana() + 20);
            System.out.println("Said juurde 25 manat! Sul on nüüd " + hero.getMana() + " manat!");
        } else if (vastane.getElupunktid() < 1) {
            hero.setMana(hero.getMana() + 10);
            System.out.println("Said juurde 10 manat! Sul on nüüd " + hero.getMana() + " manat!");
        }
    }

    /**
     * Kangelase 1-kordse maagiarünnaku meetod.
     * Tekitasin booleani, mis mängu alguses on true ning muutub false-iks, kui kasutaja otsustab ära kasutada maagiarünnaku.
     * Maagiarünnak on alati random tugevusega rünnak, mis varieerub 0st 100ni.
     * Kui kasutaja üritab maagiarünnakut kasutada isegi siis kui see on juba kasutatud, siis tuleb sellest teavitus.
     * Lisasin meetodisse veel maagiaNali meetodi, mis teeb väikse nalja kasutaja maagiarünnaku tugevuse põhjal.
     */
    public static void magicRynnak() {
        if (hero.getMaagiaRynnakuArv() && bossSpawner > 35) {
            boss.setBossElu(boss.getBossElu() - hero.getMaagiaRynnak());
            hero.setMaagiaRynnakuArv(false);
            maagiaNali();
            System.out.println("Sa kasutasid ära oma maagiarünnaku ning tegid bossile " + hero.getMaagiaRynnak() + " viga!");
            bossiUuedElud();
            if (boss.getBossElu() >= 1)
                System.out.println("Bossil on alles " + boss.getBossElu() + " elu!");
            if (boss.getBossElu() < 1)
                System.out.println("Sa tapsid bossi maagiaga!");
        } else if (hero.getMaagiaRynnakuArv()) {
            vastane.setElupunktid(vastane.getElupunktid() - hero.getMaagiaRynnak());
            hero.setMaagiaRynnakuArv(false);
            maagiaNali();
            System.out.println("Sa kasutasid ära oma maagiarünnaku ning tegid kollile " + hero.getMaagiaRynnak() + " viga!");
            if (vastane.getElupunktid() >= 1)
                System.out.println("Vastasel on alles " + vastane.getElupunktid() + " elu!");
            if (vastane.getElupunktid() < 1)
                System.out.println("Su maagiarünnak hävitas kolli täielikult");
        } else {
            System.out.println("Sinu maagiarünnak on ära kasutatud!");
        }
    }

    /**
     * Meetod, mis kontrollib kas kangelane sai surma või mitte ning annab sellest kasutajale teada.
     */
    public static void surmaKontroll() {
        if (hero.getElu() < 1) {
            System.out.println("Sa said surma!");
        }
    }

    /**
     * Meetod, mis annab kasutajale teada tema võidust kas bossi või tavalise vastase üle.
     */
    public static void võiduKontroll() {
        if (((bossSpawner > 35 && boss.getBossElu() < 1)) || ((bossSpawner < 35 && vastane.getElupunktid() < 1))) {
            uusMana();
            System.out.println("Sul vedas, kuid uus monstrum on juba ootel!");
            System.out.println("_____________________________________________________");
        }
    }

    /**
     * Meetod, mis annab kasutajale teada, kas vastane sai surma või mitte.
     * Kui ei, siis teavitab tehtud kahjust ning kui palju vastasel elusi veel on.
     */
    public static void vastaseSurmaKontroll() {
        if (vastane.getElupunktid() < 1) {
            System.out.println("Sa said kollist jagu!");
        } else {
            System.out.println("Sa ründasid kolli " + hero.getRynnak() + " võrra!");
            System.out.println("Kollil on alles " + vastane.getElupunktid() + " elu!");
        }
    }

    /**
     * Meetod, mis kontrollib ning annab kasutajale teada sellest, kui boss on surma saanud.
     * Kui ei, siis saab kasutaja jällegi teavituse bossile tehtud kahjust ning kui palju bossil elusi veel on.
     */
    public static void bossiSurmaKontroll() {
        if (boss.getBossElu() < 1) {
            System.out.println("Sa suutsid alistada bossi, tubli töö!");
        } else {
            System.out.println("Sa ründasid bossi " + hero.getRynnak() + " võrra!");
            System.out.println("Ära veel kergelt hinga, bossil on alles " + boss.getBossElu() + " elu!");
        }
    }

    /**
     * Kui kangelane saab surma, siis käivitub gameOver meetod, mis annab kasutajale valikuvõimaluse, et kas mängida uuesti või mitte.
     * Kõigepealt antakse kasutajale teada, kui palju punkte ta seekord kokku sai.
     * Lisasin meetodisse veel try ja catch execptionpüüdja, mis palub kasutajal sisestada tähemärgi asemel arvu juhul,
     * kui kasutaja peaks tähemärgi sisestama. Lisasin kontroll booleani, mis vale arvu korral läheb uuesti meetodi algusesse.
     */
    public static void gameOver() {
        while (kontroll) {
            Scanner input = new Scanner(System.in);
            System.out.println("Sa tapsid " + bossKills + " bossi ning " + vastaneKills + " vastast.");
            System.out.println("Sa said kokku " + skoor + " punkti, better luck next time!");
            System.out.println("_____________________________________________________");
            System.out.println("Kas soovid uuesti mängida?");
            System.out.println("1: Jah, proovime uuesti!");
            System.out.println("2: Ei soovi rohkem!");
            try {
                int valik = input.nextInt();
                switch (valik) {
                    case 1:
                        newGame();
                        kontroll = false;
                        break;
                    case 2:
                        System.out.println("Aitäh, et mängisid! :)");
                        kontroll = false;
                        break;
                    default:
                        System.out.println("Vale arv!");
                }
            } catch (Exception e) {
                System.out.println("Kirjuta arv, mitte midagi muud palun!");
            }
        }
    }

    /**
     * Meetod, mis käivitab mängu otsast peale juhul, kui kasutaja soovib gameOver meetodis vajutada,
     * et jah, ta soovib uuesti mängida.
     */
    public static void newGame() {
        gameStart();
        theGame();
        gameOver();
    }

    /**
     * Bossi elutagastus meetod.
     * Kuna boss on võimas olend, siis tal on võimalus ennast ka tervendada.
     * Tegin uue globaalse muutuja bossUuedElud, mille väärtus on suvaline number 0st 20ni.
     */
    public static void bossiUuedElud() {
        if (bossUuedElud > 15 && boss.getBossElu() >= 1) {
            boss.setBossElu(boss.getBossElu() + 30);
            System.out.println("Boss lõi kartma ning tervendas ennast, ta sai juurde 30 elu!");
        }
    }

    /**
     * Meetod, mis bossi korral lisab lõppskoorile 10 punkti,
     * vastase korral lisab 5 punkti.
     *
     * @return skoor;
     */
    public static int skooriArvutus() {
        if (bossSpawner > 35 && boss.getBossElu() < 1) {
            skoor += 10;
        } else if (bossSpawner < 35 && vastane.getElupunktid() < 1) {
            skoor += 5;
        }
        return skoor;
    }

    /**
     * Meetod, mis loeb mitu bossi või kolli tapnud oled.
     */
    public static void kuiPaljuTapsid() {
        if (bossSpawner > 35 && boss.getBossElu() < 1) {
            bossKills++;
        } else if (bossSpawner < 35 && vastane.getElupunktid() < 1)
            vastaneKills++;
    }

    /**
     * Meetod, mis teeb väikse nalja kangelase maagiarünnaku tugevuse suhtes.
     */
    public static void maagiaNali() {
        if (hero.getMaagiaRynnak() > 35) {
            System.out.println("Sul ikka vedas praegu maagiaga.");
        } else {
            System.out.println("See oli küll jube halb maagiarünnak, kuid pea vastu!");
        }
    }
}

// Koodi lõpp.





