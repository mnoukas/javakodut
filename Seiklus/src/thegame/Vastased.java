package thegame;

/**
 * Created by Martin on 22.11.2015.
 */

import java.util.Random;

/**
 * Vastaste jaoks loodud klass.
 */
public class Vastased {

    /**
     * Loome järjekordselt suvalise numbri generaatori objekti.
     */
    Random rand = new Random();

    /**
     * Koletiste atribuudid, kus nimed on paigutatud massiivi.
     */
    String[] olend = {"Vampire", "Zombie", "Werewolf", "Mutant", "Goblin"};
    int elupunktid;
    int tabamus;
    String nimi;

    /**
     * Vastaste klassi konstruktor, kus muutujate väärtused on määratud.
     */
    public Vastased() {
        this.nimi = olend[rand.nextInt(olend.length)];
        this.elupunktid = rand.nextInt(60) + 20;
        this.tabamus = rand.nextInt(50) + 15;
    }

    /**
     * Meetodid, mis tagastavad muutujate väärtused.
     */
    public String getNimi() {
        return this.nimi;
    }

    public int getElupunktid() {
        return this.elupunktid;
    }

    public int getTabamus() {
        return this.tabamus;
    }

    /**
     * Vastase elupunktide määrajameetod.
     * @param elupunktid
     */
    public void setElupunktid(int elupunktid) {
        this.elupunktid = elupunktid;
    }
}

// Klassi lõpp

